<h1>Read or Buy</h1>

Projekt przedstawia bazę danych połączenia księgarni z biblioteką. Klienci mogą wypożyczyć bądź
kupić książkę od wybranych autorów. Jest to o tyle wygodne że ceny używanych książek są niższe
i jeśli użytkownik będzie chciał ją kupić nie jest to problemem. Dane klientów będą
przechowywane przez 5 lat, utworzenie tabeli użytkowników daje możliwość łatwiej implementacji
do strony www.