CREATE TABLE autor
(
  id integer NOT NULL,
  imie varchar(45) NOT NULL,
  nazwisko varchar(45) NOT NULL,
  CONSTRAINT autor_pkey PRIMARY KEY (id)
);

CREATE TABLE rodz_oplat
(
  id integer NOT NULL,
  nazwa varchar(30) NOT NULL,
  ilosc integer,
  CONSTRAINT rodz_oplat_pkey PRIMARY KEY (id)
);

CREATE TABLE typ_transak
(
  id integer NOT NULL,
  nazwa varchar(30) NOT NULL,
  CONSTRAINT typ_transak_pkey PRIMARY KEY (id)
);

CREATE TABLE klient
(
  id integer NOT NULL,
  imie varchar(16) NOT NULL,
  nazwisko varchar(30) NOT NULL,
  adres varchar(50) NOT NULL,
  miasto varchar(30) NOT NULL,
  wojewodztwo varchar(30),
  kodpocztowy varchar(30),
  CONSTRAINT klient_pkey PRIMARY KEY (id)
);

CREATE TABLE uzytk
(
  id integer NOT NULL,
  login varchar(16) NOT NULL,
  haslo varchar(30) NOT NULL,
  CONSTRAINT uzytk_pkey PRIMARY KEY (id)
);

CREATE TABLE ksiazki
(
  id integer NOT NULL,
  autor_id integer NOT NULL,
  ilosc integer NOT NULL,
  cena varchar(11) NOT NULL,
  tytul varchar(40) NOT NULL,
  wydawnictwo varchar(30) NOT NULL,
  CONSTRAINT ksiazki_pkey PRIMARY KEY (id),
  CONSTRAINT fk_autor FOREIGN KEY (autor_id) REFERENCES autor (id)
);

CREATE TABLE transakcja
(
  id integer NOT NULL,
  typ_transak_id integer NOT NULL,
  uzytk_id integer,
  klient_id integer NOT NULL,
  cena varchar(10),
  data_transakcji date,
  data_realizacji date,
  CONSTRAINT transakcja_pkey PRIMARY KEY (id),
  CONSTRAINT fk_typ_transak_id FOREIGN KEY (typ_transak_id)
      REFERENCES typ_transak (id),
  CONSTRAINT fk_uzytk_id FOREIGN KEY (uzytk_id)
      REFERENCES uzytk (id),
  CONSTRAINT fk_klient_id FOREIGN KEY (klient_id)
      REFERENCES klient (id)	  
);

CREATE TABLE przed_transak
(
  id integer NOT NULL,
  transakcja_id integer NOT NULL,
  ksiazki_id integer,
  CONSTRAINT przed_transak_pkey PRIMARY KEY (id),
  CONSTRAINT fk_transakcja_id FOREIGN KEY (transakcja_id)
      REFERENCES transakcja (id),
  CONSTRAINT fk_ksiazki_id FOREIGN KEY (ksiazki_id)
      REFERENCES ksiazki (id)
);


CREATE TABLE oplaty
(
  id integer NOT NULL,
  transakcja_id integer NOT NULL,
  rodz_oplat_id integer NOT NULL,
  data_oplaty date,
  zaplata number(1),
  CONSTRAINT oplaty_pkey PRIMARY KEY (id),
  CONSTRAINT fk_transakcja_oplaty_id FOREIGN KEY (transakcja_id)
      REFERENCES transakcja (id),
  CONSTRAINT fk_rodz_oplat_id FOREIGN KEY (rodz_oplat_id)
      REFERENCES rodz_oplat (id)
);

CREATE SEQUENCE SEQ_AUTOR INCREMENT BY 1 START WITH 1 MAXVALUE 999999999 MINVALUE 1 NOCACHE ORDER;
CREATE SEQUENCE SEQ_RODZ_OPLAT INCREMENT BY 1 START WITH 1 MAXVALUE 999999999 MINVALUE 1 NOCACHE ORDER;
CREATE SEQUENCE SEQ_TYP_TRANSAK INCREMENT BY 1 START WITH 1 MAXVALUE 999999999 MINVALUE 1 NOCACHE ORDER;
CREATE SEQUENCE SEQ_KLIENT INCREMENT BY 1 START WITH 1 MAXVALUE 999999999 MINVALUE 1 NOCACHE ORDER;
CREATE SEQUENCE SEQ_UZYTK INCREMENT BY 1 START WITH 1 MAXVALUE 999999999 MINVALUE 1 NOCACHE ORDER;
CREATE SEQUENCE SEQ_KSIAZKI INCREMENT BY 1 START WITH 1 MAXVALUE 999999999 MINVALUE 1 NOCACHE ORDER;
CREATE SEQUENCE SEQ_OPLATY INCREMENT BY 1 START WITH 1 MAXVALUE 999999999 MINVALUE 1 NOCACHE ORDER;
CREATE SEQUENCE SEQ_PRZED_TRANSAK INCREMENT BY 1 START WITH 1 MAXVALUE 999999999 MINVALUE 1 NOCACHE ORDER;
CREATE SEQUENCE SEQ_TRANSAKCJA INCREMENT BY 1 START WITH 1 MAXVALUE 999999999 MINVALUE 1 NOCACHE ORDER;


create or replace trigger TR_AUTOR  
   before insert on "AUTOR" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID" is null then 
         select SEQ_AUTOR.nextval into :NEW."ID" from dual; 
      end if; 
   end if; 
end;
/
create or replace trigger TR_RODZ_OPLAT  
   before insert on "RODZ_OPLAT" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID" is null then 
         select SEQ_RODZ_OPLAT.nextval into :NEW."ID" from dual; 
      end if; 
   end if; 
end;
/
create or replace trigger TR_TYP_TRANSAK 
   before insert on "TYP_TRANSAK" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID" is null then 
         select SEQ_TYP_TRANSAK.nextval into :NEW."ID" from dual; 
      end if; 
   end if; 
end;
/
create or replace trigger TR_KLIENT
   before insert on "KLIENT" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID" is null then 
         select SEQ_KLIENT.nextval into :NEW."ID" from dual; 
      end if; 
   end if; 
end;
/
create or replace trigger TR_UZYTK
   before insert on "UZYTK" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID" is null then 
         select SEQ_UZYTK.nextval into :NEW."ID" from dual; 
      end if; 
   end if; 
end;
/
create or replace trigger TR_KSIAZKI  
   before insert on "KSIAZKI" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID" is null then 
         select SEQ_KSIAZKI.nextval into :NEW."ID" from dual; 
      end if; 
   end if; 
end;
/

create or replace trigger TR_OPLATY  
   before insert on "OPLATY" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID" is null then 
         select SEQ_OPLATY.nextval into :NEW."ID" from dual; 
      end if; 
   end if; 
end;
/
create or replace trigger TR_PRZED_TRANSAK  
   before insert on "PRZED_TRANSAK" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID" is null then 
         select SEQ_PRZED_TRANSAK.nextval into :NEW."ID" from dual; 
      end if; 
   end if; 
end;
/

create or replace trigger TR_TRANSAKCJA  
   before insert on "TRANSAKCJA" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID" is null then 
         select SEQ_TRANSAKCJA.nextval into :NEW."ID" from dual; 
      end if; 
   end if; 
end;
/

INSERT INTO autor (id, imie, nazwisko) VALUES('1', 'Henryk','Sienkiewicz');
INSERT INTO autor (id, imie, nazwisko) VALUES('2', 'John', 'Tolkien');
INSERT INTO autor (id, imie, nazwisko) VALUES('3', 'Andrzej', 'Sapkowski');
INSERT INTO autor (id, imie, nazwisko) VALUES('4', 'Stephen', 'King');
INSERT INTO autor (id, imie, nazwisko) VALUES('5', 'Terry', 'Pratchet');
INSERT INTO autor (id, imie, nazwisko) VALUES('6', 'Paulo', 'Coelho');

INSERT INTO ksiazki (id, autor_id, ilosc, cena, tytul, wydawnictwo) VALUES('1', '1', '1', '30', 'Krzyzacy', 'Operon');
INSERT INTO ksiazki (id, autor_id, ilosc, cena, tytul, wydawnictwo) VALUES('2', '3', '3', '40', 'Sillmarilion', 'Operon');
INSERT INTO ksiazki (id, autor_id, ilosc, cena, tytul, wydawnictwo) VALUES('3', '2', '2', '50', 'Krew Elfow', 'Hellion');
INSERT INTO ksiazki (id, autor_id, ilosc, cena, tytul, wydawnictwo) VALUES('4', '4', '5', '20', 'Lśnienie', 'Prószyński i S-ka');
INSERT INTO ksiazki (id, autor_id, ilosc, cena, tytul, wydawnictwo) VALUES('5', '2', '5', '30', 'Hobbit', 'Hellion');
INSERT INTO ksiazki (id, autor_id, ilosc, cena, tytul, wydawnictwo) VALUES('6', '4', '5', '30', 'Doktor Sen', 'Prószyński i S-ka');
INSERT INTO ksiazki (id, autor_id, ilosc, cena, tytul, wydawnictwo) VALUES('7', '5', '5', '20', 'Mort Tom 4', 'Prószyński i S-ka');
INSERT INTO ksiazki (id, autor_id, ilosc, cena, tytul, wydawnictwo) VALUES('8', '1', '4', '20', 'Kolor Magii Tom 1', 'Prószyński i S-ka');
INSERT INTO ksiazki (id, autor_id, ilosc, cena, tytul, wydawnictwo) VALUES('9', '4', '5', '20', 'To', 'Hellion');
INSERT INTO ksiazki (id, autor_id, ilosc, cena, tytul, wydawnictwo) VALUES('10', '6', '5', '20', 'Byc jak plynaca rzeka', 'Hellion');

INSERT INTO rodz_oplat (id, nazwa, ilosc) VALUES(1, 'oplata zalegla', 1);
INSERT INTO rodz_oplat (id, nazwa, ilosc) VALUES(2, 'zakup', 2);
INSERT INTO rodz_oplat (id, nazwa, ilosc) VALUES(3, 'karna', 1);

INSERT INTO typ_transak (id, nazwa) VALUES(1, 'oplata');
INSERT INTO typ_transak (id, nazwa) VALUES(2, 'zakup');
INSERT INTO typ_transak (id, nazwa) VALUES(3, 'wypozyczenie');

INSERT INTO uzytk (id, login, haslo) VALUES(1, 'mateusz', 'lala');
INSERT INTO uzytk (id, login, haslo) VALUES(2, 'dragon', 'lala');
INSERT INTO uzytk (id, login, haslo) VALUES(3, 'sys', 'sys2');

INSERT INTO klient (id,imie,nazwisko,adres,miasto,wojewodztwo,kodpocztowy) VALUES(1, 'Mateusz', 'Mały', 'Zamek 1', 'Sanok', 'podkarpackie', '38-500');
INSERT INTO klient (id,imie,nazwisko,adres,miasto,wojewodztwo,kodpocztowy) VALUES(2, 'Lukasz', 'Mały', 'Zamek', 'Warszawa', 'mazowieckie', '04-364');
INSERT INTO klient (id,imie,nazwisko,adres,miasto,wojewodztwo,kodpocztowy) VALUES(3, 'Lukasz', 'Wielki', 'Wiejska 4', 'Warszawa', 'mazowieckie', '02-489');

INSERT INTO transakcja (id, typ_transak_id, uzytk_id, klient_id, cena, data_transakcji, data_realizacji) VALUES(1, 1, 1, 1, 30, '2012-10-01', NULL);
INSERT INTO transakcja (id, typ_transak_id, uzytk_id, klient_id, cena, data_transakcji, data_realizacji) VALUES(2, 2, 1, 2, 100, '2012-10-16', '2012-10-17');
INSERT INTO transakcja (id, typ_transak_id, uzytk_id, klient_id, cena, data_transakcji, data_realizacji) VALUES(3, 3, 1, 1, 30, '2012-10-16', '2012-10-21');
INSERT INTO transakcja (id, typ_transak_id, uzytk_id, klient_id, cena, data_transakcji, data_realizacji) VALUES(4, 3, 1, 1, 30, '2012-10-16', '2012-10-21');
INSERT INTO transakcja (id, typ_transak_id, uzytk_id, klient_id, cena, data_transakcji, data_realizacji) VALUES(5, 3, 1, 2, 30, '2012-10-16', '2012-10-21');
INSERT INTO transakcja (id, typ_transak_id, uzytk_id, klient_id, cena, data_transakcji, data_realizacji) VALUES(6, 3, 1, 3, 30, '2012-10-16', '2012-10-21');
INSERT INTO transakcja (id, typ_transak_id, uzytk_id, klient_id, cena, data_transakcji, data_realizacji) VALUES(7, 2, 2, 3, 60, '2014-10-16', '2014-10-21');

INSERT INTO oplaty (id, transakcja_id, rodz_oplat_id, data_oplaty, zaplata) VALUES('1', '1', '1', '12/10/17', '1');
INSERT INTO oplaty (id, transakcja_id, rodz_oplat_id, data_oplaty, zaplata) VALUES('2', '2', '2', '12/10/17', '0');
INSERT INTO oplaty (id, transakcja_id, rodz_oplat_id, data_oplaty, zaplata) VALUES('3', '3', '3', '13/10/21', '1');
INSERT INTO oplaty (id, transakcja_id, rodz_oplat_id, data_oplaty, zaplata) VALUES('4', '4', '1', '14/11/21', '1');
INSERT INTO oplaty (id, transakcja_id, rodz_oplat_id, data_oplaty, zaplata) VALUES('5', '5', '2', '15/12/21', '1');
INSERT INTO oplaty (id, transakcja_id, rodz_oplat_id, data_oplaty, zaplata) VALUES('6', '6', '3', '15/12/21', '1');
INSERT INTO oplaty (id, transakcja_id, rodz_oplat_id, data_oplaty, zaplata) VALUES('7', '7', '3', '15/12/21', '0');

INSERT INTO przed_transak (id, transakcja_id, ksiazki_id) VALUES(1, 1, 1);
INSERT INTO przed_transak (id, transakcja_id, ksiazki_id) VALUES(2, 2, 3);
INSERT INTO przed_transak (id, transakcja_id, ksiazki_id) VALUES(3, 3, 2);
INSERT INTO przed_transak (id, transakcja_id, ksiazki_id) VALUES(4, 3, 4);
INSERT INTO przed_transak (id, transakcja_id, ksiazki_id) VALUES(5, 5, 3);

CREATE OR REPLACE FORCE VIEW V_KSIAZKI_KLIENTA AS 
 
select kli.imie,kli.nazwisko,kli.adres,kli.miasto,kli.wojewodztwo,kli.kodpocztowy,k.tytul

from ksiazki k, klient kli, autor a,przed_transak przed,transakcja t
where a.id = k.autor_id
and k.id=przed.ksiazki_id
and przed.transakcja_id=t.id
and t.klient_id=kli.id;
 
CREATE OR REPLACE VIEW V_TRANSAKCJE_KLIENTOW AS

select k.imie, k.nazwisko,t.cena, ks.ilosc,typ.nazwa

from klient k, transakcja t,typ_transak typ,ksiazki ks,przed_transak przed
where t.klient_id=k.id
and t.typ_transak_id=typ.id
and ks.id=przed.ksiazki_id
and przed.transakcja_id=t.id;

CREATE OR REPLACE FUNCTION ksiazkaid_max RETURN NUMBER AS
wynik NUMBER;
BEGIN
SELECT MAX(id) INTO wynik FROM ksiazki;
RETURN wynik;
END;
/

CREATE OR REPLACE FUNCTION ksiazkaid_min RETURN NUMBER AS
wynik NUMBER;
BEGIN
SELECT MIN(id) INTO wynik FROM ksiazki;
RETURN wynik;
END;
/

CREATE OR REPLACE FUNCTION random_klient_ID RETURN NUMBER AS
wynik NUMBER;
BEGIN
SELECT * INTO wynik FROM (SELECT id FROM klient ORDER BY DBMS_RANDOM.RANDOM)
WHERE ROWNUM <2;
REURN wynik;
END;
/

CREATE OR REPLACE PROCEDURE aktualizacjaksiazki (kolumna VARCHAR2,valold VARCHAR2,valnew VARCHAR2) IS
	l_prop VARCHAR2(10);
	l_valnew VARCHAR2(10);
	l_valold VARCHAR2(10);
		BEGIN
		l_prop:=kolumna;
		l_valnew:=valnew;
		l_valold:=valold;
  
			IF (upper(l_prop)='ILOSC') THEN
			update ksiazki set  ILOSC=l_valnew where ILOSC=l_valold;
			elsif (upper(l_prop)='CENA') THEN
			update ksiazki set  CENA=l_valnew where CENA=l_valold;
			elsif (upper(l_prop)='TYTUL') THEN
			update ksiazki set  TYTUL=l_valnew where TYTUL=l_valold;
			elsif (upper(l_prop)='WYDAWNICTWO') THEN
			update ksiazki set  WYDAWNICTWO=l_valnew where WYDAWNICTWO=l_valold;
			END IF;
		END;
/
commit;